</main>

<?php if (is_active_sidebar('widget-footer')) : ?>
    <div class="contact-form bg-primary container-fluid" id="contact">
        <div class="container py-4 pb-5">
            <header class="section-header mb-4 pt-3">
                <h2 class="text-white text-center text-uppercase"><?php _e("Contact us", "panama") ?></h2>
            </header>
            <?php dynamic_sidebar('widget-footer'); ?>
            <a href="#top">
                <div class="contact-form__goto-top">
                    <svg class="icon icon-arrow-up">
                        <use xlink:href="#icon-arrow-up"></use>
                    </svg>
                </div>
            </a>
        </div>
    </div>
<?php endif; ?>

<footer role="contentinfo" class="footer bg-footer pt-4">
    <div class="container">
        <p>
            <?php if (get_theme_mod('name')) : ?>
                <b><?php echo get_theme_mod('name'); ?></b><br>
            <?php endif; ?>
            <?php echo get_theme_mod('street') ?><br>
            <?php echo get_theme_mod('city') ?>
        </p>
        <p>
            <?php if (get_theme_mod('ico')) : ?>
                <b>IČO:</b> <?php echo get_theme_mod('ico') ?><br>
            <?php endif; ?>
            <?php if (get_theme_mod('dic')) : ?>
                <b>DIČ:</b> <?php echo get_theme_mod('dic') ?>
            <?php endif; ?>
        </p>
        <p>
            <?php if (get_theme_mod('phone')) : ?>
                <b><?php _e("Phone", "panama") ?>:</b> <a class="text-white" href="tel:<?php echo get_theme_mod("phone"); ?>"><?php echo get_theme_mod("phone"); ?></a><br>
            <?php endif; ?>
            <?php if (get_theme_mod('email')) : ?>
                <b><?php _e("E-mail", "panama") ?>:</b> <a class="text-white" href="mailto:<?php echo get_theme_mod("email"); ?>"><?php echo get_theme_mod("email"); ?></a><br>
            <?php endif; ?>
        </p>
    </div>
    <div class="bg-footer-dark p-4 mt-4">
        <div class="container">
            <p class="m-0">
                &copy; <?php echo esc_html(date_i18n(__('Y', 'veterani3'))); ?> <?php echo get_bloginfo("name"); ?>
            </p>
        </div>
    </div>
</footer>


<?php wp_footer(); ?>

<script type="text/javascript">
    /* <![CDATA[ */
    var seznam_retargeting_id = 100545;
    /* ]]> */
</script>
<script type="text/javascript" src="//c.imedia.cz/js/retargeting.js"></script>

</body>

</html>
