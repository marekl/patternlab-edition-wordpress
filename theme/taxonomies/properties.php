<?php
require_once __DIR__ . '/../cmb2/init.php';

add_action('init', 'panama_register_projects');
function panama_register_projects()
{
    /**
     * Post Type: Project.
     */

    $labels = [
        "name" => __("Projects", "panama"),
        "singular_name" => __("Project", "panama"),
        "menu_name" => __("Properties", "panama"),
        "all_items" => __("All projects", "panama"),
        "add_new" => __("Add new", "panama"),
        "add_new_item" => __("Add new project", "panama"),
        "edit_item" => __("Edit project", "panama"),
        "new_item" => __("New project", "panama"),
        "view_item" => __("View project", "panama"),
        "view_items" => __("View projects", "panama"),
        "search_items" => __("Search projects", "panama"),
        "not_found" => __("No projects found", "panama"),
        "not_found_in_trash" => __("No projects found in trash", "panama"),
        "parent" => __("Project:", "panama"),
        "featured_image" => __("Featured image", "panama"),
        "set_featured_image" => __("Set featured image", "panama"),
        "remove_featured_image" => __("Remove featured image", "panama"),
        "use_featured_image" => __("Use as featured image", "panama"),
        "archives" => __("Archives of projects", "panama"),
        "insert_into_item" => __("Insert into project", "panama"),
        "uploaded_to_this_item" => __("Upload to this project", "panama"),
        "filter_items_list" => __("Filter list of projects", "panama"),
        "items_list_navigation" => __("Project list navigation", "panama"),
        "items_list" => __("Project list", "panama"),
        "attributes" => __("Attributes", "panama"),
        "name_admin_bar" => __("Project", "panama"),
        "item_published" => __("Project has been published", "panama"),
        "item_published_privately" => __("Project has been published privately", "panama"),
        "item_reverted_to_draft" => __("Project was reverted to draft", "panama"),
        "item_scheduled" => __("Project scheduled", "panama"),
        "item_updated" => __("Project updated", "panama"),
        "parent_item_colon" => __("Project:", "panama"),
    ];

    $args = [
        "label" => __("Project", "panama"),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "delete_with_user" => false,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => ["slug" => "project", "with_front" => true],
        "query_var" => true,
        "menu_icon" => "data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjMyIiBoZWlnaHQ9IjMyIiB2aWV3Qm94PSIwIDAgMzIgMzIiPgo8cGF0aCBmaWxsPSIjZmZmIiBkPSJNMzIgMTguNDUxbC0xNi0xMi40Mi0xNiAxMi40MnYtNS4wNjRsMTYtMTIuNDIgMTYgMTIuNDJ6TTI4IDE4djEyaC04di04aC04djhoLTh2LTEybDEyLTl6Ij48L3BhdGg+PC9zdmc+",
        "supports" => ["title", "thumbnail", "editor", "excerpt", "revisions"],
    ];

    register_post_type("project", $args);
}

add_action('cmb2_admin_init', 'panama_register_project_box');
function panama_register_project_box()
{

    $subtitle_box = new_cmb2_box(array(
        'id'               => 'panama_subtitle',
        'title'            => esc_html__('Subtitle', 'panama'),
        'context'          => 'side',
        'remove_box_wrap'  => true,
        'priority'         => 'high',
        'object_types'      => array('project'),
        'show_names'        => false
    ));

    $subtitle_box->add_field(array(
        'name'             => __('Subtitle', 'panama'),
        'id'               => 'subtitle',
        'type'             => 'text',
        'attributes'       => array(
            'style'    => 'width: 100%',
        ),
    ));

    $values_box = new_cmb2_box(array(
        'id'               => 'panama_values',
        'title'            => esc_html__('Attributes', 'panama'),
        'context'          => 'side',
        'remove_box_wrap'  => true,
        'priority'         => 'high',
        'object_types'      => array('project'),
        'show_names'        => true
    ));

    $values_box->add_field(array(
        'name'             => __('Location', 'panama'),
        'id'               => 'panama_location',
        'type'             => 'text',
        'attributes'       => array(
            'style'    => 'width: 100%',
        ),
    ));

    $values_box->add_field(array(
        'name'             => __('Area A', 'panama'),
        'id'               => 'panama_areaA',
        'type'             => 'text',
        'attributes'       => array(
            'style'    => 'width: 100%',
        ),
    ));

    $values_box->add_field(array(
        'name'             => __('Price A (USD)', 'panama'),
        'id'               => 'panama_priceA',
        'type'             => 'text',
        'attributes'       => array(
            'style'    => 'width: 100%',
        ),
    ));

    $values_box->add_field(array(
        'name'             => __('Area B', 'panama'),
        'id'               => 'panama_areaB',
        'type'             => 'text',
        'attributes'       => array(
            'style'    => 'width: 100%',
        ),
    ));

    $values_box->add_field(array(
        'name'             => __('Price B (USD)', 'panama'),
        'id'               => 'panama_priceB',
        'type'             => 'text',
        'attributes'       => array(
            'style'    => 'width: 100%',
        ),
    ));

    $values_box->add_field(array(
        'name'             => __('Area C', 'panama'),
        'id'               => 'panama_areaC',
        'type'             => 'text',
        'attributes'       => array(
            'style'    => 'width: 100%',
        ),
    ));

    $values_box->add_field(array(
        'name'             => __('Price C (USD)', 'panama'),
        'id'               => 'panama_priceC',
        'type'             => 'text',
        'attributes'       => array(
            'style'    => 'width: 100%',
        ),
    ));

    // $box1 = new_cmb2_box(array(
    //     'id'               => 'panama_project',
    //     'title'            => esc_html__('Project', 'panama'),
    //     'object_types'     => array('project'),
    //     'priority'         => 'low',
    //     'show_names'       => true
    // ));


    $box3 = new_cmb2_box(array(
        'id'                => 'panama_gallery',
        'title'             => __('Gallery', 'panama'),
        'priority'         => 'low',
        'object_types'      => array('project'),
        'show_names'        => false
    ));

    $box3->add_field(array(
        'name' => __('Gallery', 'panama'),
        'id'   => 'panama_gallery_files',
        'type' => 'file_list',
        'preview_size' => array(200, 200),
        'query_args' => array('type' => 'image'),
    ));
}
