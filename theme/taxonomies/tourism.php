<?php
require_once __DIR__ . '/../cmb2/init.php';

add_action('init', 'panama_register_tourism_projects');
function panama_register_tourism_projects()
{
    /**
     * Post Type: Tourism.
     */

    $labels = [
        "name" => __("Projects", "panama"),
        "singular_name" => __("Project", "panama"),
        "menu_name" => __("Tourism", "panama"),
        "all_items" => __("All projects", "panama"),
        "add_new" => __("Add new", "panama"),
        "add_new_item" => __("Add new project", "panama"),
        "edit_item" => __("Edit project", "panama"),
        "new_item" => __("New project", "panama"),
        "view_item" => __("View project", "panama"),
        "view_items" => __("View projects", "panama"),
        "search_items" => __("Search projects", "panama"),
        "not_found" => __("No projects found", "panama"),
        "not_found_in_trash" => __("No projects found in trash", "panama"),
        "parent" => __("Project:", "panama"),
        "featured_image" => __("Featured image", "panama"),
        "set_featured_image" => __("Set featured image", "panama"),
        "remove_featured_image" => __("Remove featured image", "panama"),
        "use_featured_image" => __("Use as featured image", "panama"),
        "archives" => __("Archives of projects", "panama"),
        "insert_into_item" => __("Insert into project", "panama"),
        "uploaded_to_this_item" => __("Upload to this project", "panama"),
        "filter_items_list" => __("Filter list of projects", "panama"),
        "items_list_navigation" => __("Project list navigation", "panama"),
        "items_list" => __("Project list", "panama"),
        "attributes" => __("Attributes", "panama"),
        "name_admin_bar" => __("Project", "panama"),
        "item_published" => __("Project has been published", "panama"),
        "item_published_privately" => __("Project has been published privately", "panama"),
        "item_reverted_to_draft" => __("Project was reverted to draft", "panama"),
        "item_scheduled" => __("Project scheduled", "panama"),
        "item_updated" => __("Project updated", "panama"),
        "parent_item_colon" => __("Project:", "panama"),
    ];

    $args = [
        "label" => __("Tourism", "panama"),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "delete_with_user" => false,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => ["slug" => "tourism", "with_front" => true],
        "query_var" => true,
        "menu_icon" => "data:image/svg+xml;base64,PHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjMyIiBoZWlnaHQ9IjMyIiB2aWV3Qm94PSIwIDAgMzIgMzIiPgo8cGF0aCBmaWxsPSIjZmZmIiBkPSJNMjQgMTkuOTk5bC01LjcxMy01LjcxMyAxMy43MTMtMTAuMjg2LTQtNC0xNy4xNDEgNi44NTgtNS4zOTctNS4zOTdjLTEuNTU2LTEuNTU2LTMuNzI4LTEuOTI4LTQuODI4LTAuODI4cy0wLjcyNyAzLjI3MyAwLjgyOCA0LjgyOGw1LjM5NiA1LjM5Ni02Ljg1OCAxNy4xNDMgNCA0IDEwLjI4Ny0xMy43MTUgNS43MTMgNS43MTN2Ny45OTloNGwyLTYgNi0ydi00bC03Ljk5OSAweiI+PC9wYXRoPjwvc3ZnPg==",
        "supports" => ["title", "thumbnail", "editor", "excerpt", "revisions"],
    ];

    register_post_type("tourism", $args);
}

add_action('cmb2_admin_init', 'panama_register_tourism_box');
function panama_register_tourism_box()
{

    $subtitle_box = new_cmb2_box(array(
        'id'               => 'panama_tourism_subtitle',
        'title'            => esc_html__('Subtitle', 'panama'),
        'context'          => 'side',
        'remove_box_wrap'  => true,
        'priority'         => 'high',
        'object_types'      => array('tourism'),
        'show_names'        => false
    ));

    $subtitle_box->add_field(array(
        'name'             => __('Subtitle', 'panama'),
        'id'               => 'subtitle',
        'type'             => 'text',
        'attributes'       => array(
            'style'    => 'width: 100%',
        ),
    ));

    $values_box = new_cmb2_box(array(
        'id'               => 'panama_tourism_values',
        'title'            => esc_html__('Attributes', 'panama'),
        'context'          => 'side',
        'remove_box_wrap'  => true,
        'priority'         => 'high',
        'object_types'      => array('tourism'),
        'show_names'        => true
    ));

    $values_box->add_field(array(
        'name'             => __('Location', 'panama'),
        'id'               => 'panama_location',
        'type'             => 'text',
        'attributes'       => array(
            'style'    => 'width: 100%',
        ),
    ));

    $values_box->add_field(array(
        'name'             => __('Area A', 'panama'),
        'id'               => 'panama_areaA',
        'type'             => 'text',
        'attributes'       => array(
            'style'    => 'width: 100%',
        ),
    ));

    $values_box->add_field(array(
        'name'             => __('Price A (USD)', 'panama'),
        'id'               => 'panama_priceA',
        'type'             => 'text',
        'attributes'       => array(
            'style'    => 'width: 100%',
        ),
    ));

    $values_box->add_field(array(
        'name'             => __('Area B', 'panama'),
        'id'               => 'panama_areaB',
        'type'             => 'text',
        'attributes'       => array(
            'style'    => 'width: 100%',
        ),
    ));

    $values_box->add_field(array(
        'name'             => __('Price B (USD)', 'panama'),
        'id'               => 'panama_priceB',
        'type'             => 'text',
        'attributes'       => array(
            'style'    => 'width: 100%',
        ),
    ));

    $values_box->add_field(array(
        'name'             => __('Area C', 'panama'),
        'id'               => 'panama_areaC',
        'type'             => 'text',
        'attributes'       => array(
            'style'    => 'width: 100%',
        ),
    ));

    $values_box->add_field(array(
        'name'             => __('Price C (USD)', 'panama'),
        'id'               => 'panama_priceC',
        'type'             => 'text',
        'attributes'       => array(
            'style'    => 'width: 100%',
        ),
    ));

    // $box1 = new_cmb2_box(array(
    //     'id'               => 'panama_project',
    //     'title'            => esc_html__('Project', 'panama'),
    //     'object_types'     => array('project'),
    //     'priority'         => 'low',
    //     'show_names'       => true
    // ));


    $box3 = new_cmb2_box(array(
        'id'                => 'panama_tourism_gallery',
        'title'             => __('Gallery', 'panama'),
        'priority'         => 'low',
        'object_types'      => array('tourism'),
        'show_names'        => false
    ));

    $box3->add_field(array(
        'name' => __('Gallery', 'panama'),
        'id'   => 'panama_gallery_files',
        'type' => 'file_list',
        'preview_size' => array(200, 200),
        'query_args' => array('type' => 'image'),
    ));
}
