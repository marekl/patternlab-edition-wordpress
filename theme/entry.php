    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

        <?php if (is_singular()) :
            get_template_part('entry', 'header');
        else :
        ?>
            <header class="article-header">
                <h3 class="entry-title">
                    <a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark"><?php the_title(); ?></a>
                    <?php edit_post_link(); ?>
                </h3>

                <?php if (!is_search()) {
                    //get_template_part('entry', 'meta');
                } ?>
            </header>

        <?php endif; ?>

        <?php get_template_part('entry', (is_front_page() || is_home() || is_front_page() && is_home() || is_archive() || is_search() ? 'summary' : 'content')); ?>
        <?php if (is_singular()) {
            get_template_part('entry-footer');
        } ?>

    </article>
