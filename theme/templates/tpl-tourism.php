<?php /* Template Name: Tourism */ ?>

<?php get_header(); ?>

<?php $values = get_theme_mod("values"); ?>

<?php if (have_posts()) : ?>
    <div class="container-fluid py-5">
        <div class="container">
            <?php while (have_posts()) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <?php the_content(); ?>
                </article>
            <?php endwhile; ?>
        </div>
    </div>
<?php endif; ?>

<?php
$projects = new WP_Query(["post_type" => "tourism"]);
if ($projects->have_posts()) : ?>
    <div class="bg-complementary container-fluid py-5">
        <div class="container">
            <header class="section-header mb-3">
                <h2><?php _e("Destinations", "panama"); ?></h2>
            </header>
            <div class="row">
                <?php while ($projects->have_posts()) : $projects->the_post(); ?>
                    <div class="col-md-4 mb-4">
                        <div class="card border-0 shadow-sm shadow-hover flex-grow-1 h-100">
                            <?php the_post_thumbnail('medium-thumbnail', ['class' => 'card-img-top']); ?>
                            <div class="card-body d-flex flex-column">
                                <h5 class="card-title"><?php the_title(); ?></h5>
                                <p class="card-text flex-grow-1">
                                    <?php the_excerpt(); ?> </p>
                                <p class="card-text text-right">
                                    <a href="<?php the_permalink(); ?>" class="btn btn-primary"><?php _e("See more", "panama"); ?></a>
                                </p>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if (get_theme_mod('name')) : ?>
    <section class="container py-5">
        <header class="section-header mb-4">
            <h2><?php _e("Contact Us", "panama") ?></h2>
        </header>

        <div class="row">
            <div class="col-12 col-md-6 order-md-2">
                <p>
                    <?php if (get_theme_mod('name')) : ?>
                        <b><?php echo get_theme_mod('name'); ?></b><br>
                    <?php endif; ?>
                    <?php echo get_theme_mod('street') ?><br>
                    <?php echo get_theme_mod('city') ?>
                </p>
                <p>
                    <?php if (get_theme_mod('ico')) : ?>
                        <b>IČO:</b> <?php echo get_theme_mod('ico') ?><br>
                    <?php endif; ?>
                    <?php if (get_theme_mod('dic')) : ?>
                        <b>DIČ:</b> <?php echo get_theme_mod('dic') ?>
                    <?php endif; ?>
                </p>
                <p>
                    <?php if (get_theme_mod('phone')) : ?>
                        <b><?php _e("Phone", "panama") ?>:</b> <a href="tel:<?php echo get_theme_mod("phone"); ?>"><?php echo get_theme_mod("phone"); ?></a><br>
                    <?php endif; ?>
                    <?php if (get_theme_mod('email')) : ?>
                        <b><?php _e("E-mail", "panama") ?>:</b> <a href="mailto:<?php echo get_theme_mod("email"); ?>"><?php echo get_theme_mod("email"); ?></a><br>
                    <?php endif; ?>
                </p>
            </div>
            <div class="col-12 mb-3 mb-md-0 col-md-6">
                <?php if (get_theme_mod('street') && get_theme_mod('city')) : ?>
                    <iframe src="https://google.com/maps/embed/v1/place?key=AIzaSyB8vQYTGsbzBi9gVn0lIKDya7c5LyuQOkU&zoom=14&q=<?php echo urlencode(get_theme_mod('street') . ", " . get_theme_mod('city')) ?>" width="100%" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0" class="rounded shadow-sm contact-map"></iframe>
                <?php endif; ?>
            </div>
        </div>
    </section>
<?php endif; ?>

<?php get_sidebar(); ?>
<?php get_footer(); ?>
