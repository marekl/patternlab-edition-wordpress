<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width" />
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <?php get_template_part("icons"); ?>

    <header role="banner" class="
        <?php echo is_front_page() ? "homepage-header " : ""; ?>
        main-header
    " itemscope itemtype="http://schema.org/LocalBusiness">

        <div class="top-bar">
            <div class="top-bar__content">
                <div class="h2 mb-0 text-white">
                    <a itemprop="name" href="<?php echo esc_url(home_url('/')); ?>" title="<?php echo esc_html(get_bloginfo('name')); ?>" rel="home" class="text-white">
                        <?php echo format_top_title(get_bloginfo("name")); ?>
                    </a>
                </div>

                <div class="text-white d-none d-xl-block">
                    <?php panama_language_switcher(true) ?>
                </div>

                <div class="nav-container">
                    <nav role="menubar" class="menubar">
                        <div class="nav-toggle">
                            <button class="btn nav-toggle-btn" type="button" aria-haspopup="true" aria-controls="menu1">
                                <div class="nav-toggle-btn-open"><span class="mr-3 font-weight-bold">MENU</span><svg class="icon icon-menu">
                                        <use xlink:href="#icon-menu"></use>
                                    </svg></div>
                                <div class="nav-toggle-btn-close"><span class="mr-3 font-weight-bold">ZAVŘÍT</span><svg class="icon icon-close">
                                        <use xlink:href="#icon-close"></use>
                                    </svg></div>
                            </button>
                        </div>

                        <?php wp_nav_menu(array(
                            'theme_location' => 'main-menu',
                            'container' => false,
                            'menu_id' => 'nav',
                            'menu_class' => 'nav nav-main',
                        )); ?>

                    </nav>
                </div>
            </div>
        </div>

        <div class="main-header__content">
            <?php if (get_page_template_slug() == "templates/tpl-tourism.php" && get_theme_mod("tourism_header_image")) : ?>
                <div class="main-header__image" style="background-image: url('<?php echo wp_get_attachment_url(get_theme_mod("tourism_header_image")); ?>');"></div>
            <?php elseif (!is_front_page() && has_post_thumbnail()) : ?>
                <div class="main-header__image" style="background-image: url('<?php echo the_post_thumbnail_url('featured-header
                '); ?>');"></div>
            <?php elseif (get_theme_mod("header_image")) : ?>
                <div class="main-header__image" style="background-image: url('<?php echo wp_get_attachment_url(get_theme_mod("header_image")); ?>');"></div>
            <?php else : ?>
                <div class="main-header__image" style="background-image: url('<?php echo get_template_directory_uri(); ?>/images/header.jpg');"></div>
            <?php endif; ?>
            <div class="main-header__bar pb-5 pb-md-4 pt-2">
                <div class="container d-flex flex-column flex-md-row justify-content-between align-items-md-center">
                    <?php if (is_front_page()) : ?>
                        <div class="main-header__title-box mb-4 mb-md-0">
                            <?php if (get_theme_mod("home_title")) : ?>
                                <h1 class="title title--main"><?php echo get_theme_mod("home_title"); ?></h1>
                            <?php else : ?>
                                <h1 class="title title--main"><?php echo get_bloginfo("name"); ?></h1>
                            <?php endif; ?>
                            <div class="subtitle"><?php echo get_bloginfo("description"); ?></div>
                        </div>
                        <div class="main-header__contact-box">
                            <?php if (get_theme_mod('phone')) : ?>
                                <div class="mb-1">
                                    <div class="main-header__contact-icon">
                                        <svg class="icon icon-phone">
                                            <use xlink:href="#icon-phone"></use>
                                        </svg>
                                    </div>
                                    <a class="text-white" href="tel:<?php echo get_theme_mod("phone"); ?>"><?php echo get_theme_mod("phone"); ?></a>
                                </div>
                            <?php endif; ?>
                            <?php if (get_theme_mod('email')) : ?>
                                <div>
                                    <div class="main-header__contact-icon">
                                        <svg class="icon icon-email">
                                            <use xlink:href="#icon-email"></use>
                                        </svg>
                                    </div>
                                    <a class="text-white" href="mailto:<?php echo get_theme_mod("email"); ?>"><?php echo get_theme_mod("email"); ?></a>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php else : ?>
                        <h1 class="title">
                            <?php the_title(); ?>
                        </h1>
                    <?php endif; ?>
                </div>
            </div>
    </header>
    <main class="main">
