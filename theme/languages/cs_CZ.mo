��    T      �  q   \            !     /     8     @     P     ]     r     w     ~     �  
   �     �     �  
   �  
   �     �     �     �     �     �     �     �          .     B     J  
   Q     \     e     u     �     �     �     �     �  	   �     �     �     �     �  	   	     	  ;   %	     a	     h	     n	     {	     �	     �	     �	     �	     �	     �	  $   �	     
     
     -
     ?
     O
     m
     v
  
   
     �
     �
     �
     �
     �
     �
  5   �
     &     -     6  /   ;     k     q     y     �     �     �     �     �     �      �  �       �     �     �     �     �     �     �                         (     9     A     R     c  	   h     r     y     �     �     �     �     �     �  	   �     �          (     C     \     b     v     {     �     �     �     �  !   �  )   �  
          &   ,     S     Z     b     u     �     �     �     �     �     �  "   �     �               7  "   K     n     w     �     �     �      �     �     �     �  b        q  
   w     �  .   �     �     �     �     �     �          %     -     >      P            *   	                D          /      P                 Q          
   1          )          6   R   <   S   0   4           :   "      8   7           N      ;       5       K   O      9   >   #           H   E         G   -   $       %   I         ,       L          ?   J             .   B   A              C       M   T           +      2   =          !   &       3   F             (   '          @           %s Return to  About Us Add new Add new project All projects Archives of projects Area Area A Area B Area C Attributes Business name Contact Contact Us Contact us DIČ Destinations E-mail Edit project Email Featured image Featured image (header) Filter list of projects For Sale & Projects Gallery Header Home image Homepage Homepage header Homepage title Icon Insert into project IČO Location Lukáš Marek Main Menu Medium thumbnail New project No projects found No projects found in trash Not Found Nothing Found Nothing found for the requested page. Try a search instead? Panama Phone Phone number Postal code and city Price Price A (USD) Price B (USD) Price C (USD) Project Project has been published Project has been published privately Project list Project list navigation Project scheduled Project updated Project was reverted to draft Project: Projects Properties Remove featured image Return to %s Search Results for: %s Search projects See more Set featured image Sorry, nothing matched your search. Please try again. Street Subtitle Text Theme for presentation of the Panama properties Title Tourism Tourism page header Upload to this project Use as featured image Value Values View project View projects https://gitlab.com/marekl/panama Project-Id-Version: Panama 1.0.0
Report-Msgid-Bugs-To: https://wordpress.org/support/theme/panama
PO-Revision-Date: 2020-11-28 00:16+0100
Last-Translator: Lukáš Marek <lukas.marek@mail.com>
Language-Team: 
Language: cs_CZ
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
X-Domain: panama
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n>=2 && n<=4 ? 1 : 2);
 %S Zpět na  O nás Přidat Přidat projekt Všechny projekty Archiv projektů Rozloha Oblast A Oblast B Oblast C Atributy Obchodní název Kontakt Kontaktujte nás Kontaktujte nás DIČ Destinace E-mail Editovat projekt Email Titulní obrázek Vybraný obrázek (hlavička) Filtrovat projekty Na prodej & projekty Galerie Hlavička Obrázek na hlavní stránce Hlavní stránka Hlavička hlavní stránky Titulek hlavní stránky Ikona Vložit do projektu IČO Lokalita Lukáš Marek Hlavní menu Střední miniatura Nový projekt Žádné projekty nebyly nalezeny Žádné projekty nebyly nalezeny v koši Nenalezeno Nic nebylo nalezeno Požadovaná stránka nebyla nalezena. Panama Telefon Telefonní číslo PSČ a město Cena Cena A (USD) Cena B (USD) Cena C (USD) Projekt Projekt byl publikován Projekt byl publikován privátně Seznam projektů Navigace seznamu projektů Projekt byl naplánován Projekt byl upraven Projekt byl navrácen do konceptů Projekt: Projekty Nemovitosti Odstranit titulní obrázek Zpět na %s Výsledky vyhledávání pro: %s Hledat projekt Zobrazit více Nastavit titulní obrázek Omlouváme se, ale pro zadaný dotaz nebyly nalezeny žádné výsledky. Prosím, zkuste to znovu. Ulice Podtitulek Text Šablona pro prezentaci nemovitostí v Panamě TItulek Turismus Hlavička stránky turismu Nahrát do tohoto projektu Použít jako titulní obrázek Hodnota Hodnoty Zobrazit projekt Zobrazit projekty https://gitlab.com/marekl/panama 