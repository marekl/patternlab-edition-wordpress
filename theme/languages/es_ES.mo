��    Q      �  m   ,      �     �     �     �                     2     7     >     E  
   L     W     e  
   m  
   x     �     �     �     �     �     �     �     �     �          
  
             %     5     D     I     ]     b  	   k     u     �     �     �  	   �     �  ;   �     	     	     &	     ;	     A	     O	     ]	     k	     s	  $   �	     �	     �	     �	     �	     �	     
     !
  
   *
     5
     K
     X
     o
     
     �
  5   �
     �
     �
     �
  /   �
               $     8     O     e     k     r       ^  �     �     �               !     5  
   K     V     ^     f  	   n     x     �     �     �     �     �     �     �     �     �     �          '     <  
   E     P     o  "   �     �     �     �     �  
   �     �                !  (   =  
   f     q  ;   �  	   �     �     �     �     �     
          (     1  ,   O     |  "   �     �     �  .   �  	   
  	             -     G     S     r     �     �  @   �     �  
   �       4   	  
   >     I  #   Q     u     �     �     �     �     �        ?          Q           @                  3   9   1          /      +          !   %          =   M   (   H                2       	   
         J   D           B   #   $   7   &              -      4                               .   ;           0   5       <   :      E      ,   >   I   8   F          '   *   K       "      6         )              C   A   G   L         N   P       O               %s Return to  About Us Add new Add new project All projects Archives of projects Area Area A Area B Area C Attributes Business name Contact Contact Us Contact us DIČ Destinations E-mail Edit project Email Featured image Featured image (header) Filter list of projects For Sale & Projects Gallery Header Home image Homepage Homepage header Homepage title Icon Insert into project IČO Location Main Menu Medium thumbnail New project No projects found No projects found in trash Not Found Nothing Found Nothing found for the requested page. Try a search instead? Phone Phone number Postal code and city Price Price A (USD) Price B (USD) Price C (USD) Project Project has been published Project has been published privately Project list Project list navigation Project scheduled Project updated Project was reverted to draft Project: Projects Properties Remove featured image Return to %s Search Results for: %s Search projects See more Set featured image Sorry, nothing matched your search. Please try again. Street Subtitle Text Theme for presentation of the Panama properties Title Tourism Tourism page header Upload to this project Use as featured image Value Values View project View projects Project-Id-Version: 
Report-Msgid-Bugs-To: https://wordpress.org/support/theme/panama
PO-Revision-Date: 
Last-Translator: Lukáš Marek <lukas.marek@mail.com>
Language-Team: 
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.4.2
Plural-Forms: nplurals=2; plural=(n != 1);
 %s Return to Sobre nosotros Añadir Agregar proyecto Todos los proyectos Archivos de proyectos Extensión Área A Área B Área C Atributos Nombre del Negocio Contacto Contáctenos Contáctenos DIČ Destinos E-mail Editar proyecto Email Imagen principal Imagen destacada (Encabezado) Filtrar por lista de proyectos En venta & proyectos Galería Encabezado Imagen en la página principal Página principal Encabezado de la página principal Título de la página principal Icono Insertar en proyecto IČO Ubicación Oferta principal Miniatura mediana Nuevo proyecto No se encontraron proyectos No se encontraron proyectos en la basura Extraviado Nada Encontrado No se encontró la página solicitada. Intenta encontrarla. Teléfono Número de teléfono Código postal y ciudad Precio Precio A (USD) Precio B (USD) Precio C (USD) Proyecto El proyecto ha sido publicado El proyecto se ha publicado de forma privada Lista de proyectos Navegación por lista de proyectos Proyecto programado Proyecto actualizado El proyecto se volvió a convertir en borrador Proyecto: Proyectos Bienes raíces Eliminar imagen principal Volver a %s Resultados de búsqueda de: %s Buscar un proyecto Mostrar más Establecer la imagen principal Disculpas, su búsqueda no tuvo resultados. Inténtalo de nuevo. Calle Subtitular Texto Plantilla para presentación inmobiliaria en Panamá Subtítulo Turismo Encabezado de la página de turismo Subir a este proyecto Usar como imagen destacada Valore Valores Mostrar proyecto Mostrar proyectos 