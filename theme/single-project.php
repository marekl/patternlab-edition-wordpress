<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <section class="bg-complementary container-fluid">
            <div class="container py-4">
                <div class="row">
                    <div class="col-12 col-lg-6 mt-3 order-lg-2">
                        <div class="row justify-content-center">
                            <div class="col-12 col-lg-10 d-flex flex-wrap justify-content-center justify-content-lg-between align-items-baseline">
                                <?php if (get_post_meta(get_the_ID(), "panama_location", 1)) : ?>
                                    <div class="property-attr mx-3 mb-2">
                                        <div class="property-attr__icon">
                                            <svg class="icon icon-location">
                                                <use xlink:href="#icon-location"></use>
                                            </svg>
                                        </div>
                                        <div class="property-attr__desc">
                                            <?php _e("Location", "panama"); ?>
                                        </div>
                                        <div class="property-attr__value">
                                            <?php echo get_post_meta(get_the_ID(), "panama_location", 1); ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if (get_post_meta(get_the_ID(), "panama_priceA", 1)) : ?>
                                    <div class="property-attr mx-3 mb-2">
                                        <div class="property-attr__icon">
                                            <svg class="icon icon-price">
                                                <use xlink:href="#icon-price"></use>
                                            </svg>
                                        </div>
                                        <div class="property-attr__desc">
                                            <?php if (get_post_meta(get_the_ID(), "panama_areaA", 1)) : ?>
                                                <?php echo get_post_meta(get_the_ID(), "panama_areaA", 1); ?>
                                            <?php else : ?>
                                                <?php _e("Price", "panama"); ?>
                                            <?php endif; ?>
                                        </div>
                                        <div class="property-attr__value">
                                            <?php echo get_post_meta(get_the_ID(), "panama_priceA", 1); ?> USD
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if (get_post_meta(get_the_ID(), "panama_priceB", 1)) : ?>
                                    <div class="property-attr mx-3 mb-2">
                                        <div class="property-attr__icon">
                                            <svg class="icon icon-price">
                                                <use xlink:href="#icon-price"></use>
                                            </svg>
                                        </div>
                                        <div class="property-attr__desc">
                                            <?php if (get_post_meta(get_the_ID(), "panama_areaB", 1)) : ?>
                                                <?php echo get_post_meta(get_the_ID(), "panama_areaB", 1); ?>
                                            <?php else : ?>
                                                <?php _e("Price", "panama"); ?>
                                            <?php endif; ?>
                                        </div>
                                        <div class="property-attr__value">
                                            <?php echo get_post_meta(get_the_ID(), "panama_priceB", 1); ?> USD
                                        </div>
                                    </div>
                                <?php endif; ?>
                                <?php if (get_post_meta(get_the_ID(), "panama_priceC", 1)) : ?>
                                    <div class="property-attr mx-3 mb-2">
                                        <div class="property-attr__icon">
                                            <svg class="icon icon-price">
                                                <use xlink:href="#icon-price"></use>
                                            </svg>
                                        </div>
                                        <div class="property-attr__desc">
                                            <?php if (get_post_meta(get_the_ID(), "panama_areaC", 1)) : ?>
                                                <?php echo get_post_meta(get_the_ID(), "panama_areaC", 1); ?>
                                            <?php else : ?>
                                                <?php _e("Price", "panama"); ?>
                                            <?php endif; ?>
                                        </div>
                                        <div class="property-attr__value">
                                            <?php echo get_post_meta(get_the_ID(), "panama_priceC", 1); ?> USD
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-6 mt-4 mt-lg-0">
                        <h3 class="text-uppercase"><?php get_subtitle() ? the_subtitle() : the_title(); ?></h3>
                        <?php the_content(); ?>
                        <p>
                            <a href="#contact" class="btn btn-primary btn-sm mt-3">I am interested</a>
                        </p>
                    </div>
                </div>
        </section>

        <?php if (get_post_meta(get_the_ID(), "panama_gallery_files", 1)) : ?>
            <section class="container-fluid py-4">
                <div class="container">
                    <header class="section-header mb-4">
                        <h2><?php _e("Gallery", "panama"); ?></h2>
                    </header>
                    <?php the_gallery(); ?>
                </div>
            </section>

        <?php endif; ?>

        <?php if (comments_open() && !post_password_required()) {
            comments_template('', true);
        } ?>
<?php endwhile;
endif; ?>
<footer class="footer">
    <?php get_template_part('nav', 'below-single'); ?>
</footer>


<?php get_sidebar(); ?>
<?php get_footer(); ?>
