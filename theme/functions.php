<?php

// CMB2 init
require_once __DIR__ . '/cmb2/init.php';
require_once __DIR__ . '/includes/kirki/kirki.php';
require_once __DIR__ . '/taxonomies/properties.php';
require_once __DIR__ . '/taxonomies/tourism.php';

$icons = array(
    "" => "",
    "phone"       => __("Phone", "panama"),
    "email"       => __("E-mail", "panama"),
    "location"    => __("Location", "panama"),
    "price"       => __("Price", "panama"),
    "area"        => __("Area", "panama")
);

define('HOURS', 60 * 60);

add_action('after_setup_theme', 'panama_setup');
function panama_setup()
{
    load_theme_textdomain('panama', get_template_directory() . '/languages');

    add_theme_support('title-tag');
    add_theme_support('automatic-feed-links');
    add_theme_support('post-thumbnails');
    add_theme_support('html5', array('search-form'));

    global $content_width;
    if (!isset($content_width)) {
        $content_width = 1920;
    }

    register_nav_menus(array('main-menu' => esc_html__('Main Menu', 'panama')));
    add_image_size('medium-thumbnail', 350, 350, true);
    add_image_size('featured-header', 1920, 400, true);
}

// Allow editors to see access the Menus page under Appearance but hide other options
// Note that users who know the correct path to the hidden options can still access them
function editors_menu()
{
    $user = wp_get_current_user();

    // Check if the current user is an Editor
    if (in_array('editor', (array) $user->roles)) {

        // They're an editor, so grant the edit_theme_options capability if they don't have it
        if (!current_user_can('edit_theme_options') || !current_user_can('manage_options')) {
            $role = get_role('editor');
            $role->add_cap('manage_options');
            $role->add_cap('edit_theme_options');
        }

        remove_menu_page('tools.php');
        remove_menu_page('options-general.php');
        remove_menu_page('edit.php?post_type=acf-field-group');
        remove_menu_page('wpcf7');
        remove_menu_page('wp-smtp/wp-smtp.php');
        remove_submenu_page('mlang', 'mlang');
        remove_submenu_page('mlang', 'mlang_settings');
        remove_submenu_page('mlang', 'mlang_wizard');
        remove_submenu_page('mlang', 'mlang_lingotek');


        // Hide the Themes page
        remove_submenu_page('themes.php', 'themes.php');

        // Hide the Widgets page
        remove_submenu_page('themes.php', 'widgets.php');
    }
}

add_action('admin_menu', 'editors_menu', 10);

function panama_kirki_configuration()
{
    return array('url_path' => get_stylesheet_directory_uri() . '/includes/kirki/');
}
add_filter('kirki/config', 'panama_kirki_configuration');

add_filter('image_size_names_choose', 'panama_image_size_names_choose');
function panama_image_size_names_choose($sizes)
{
    return array_merge($sizes, array(
        'medium-thumbnail' => __('Medium thumbnail', 'panama'),
        'featured-header' => __('Featured image (header)', 'panama'),
    ));
}


add_filter('wp_nav_menu_items', 'panama_contact_menu_item', 10, 2);
function panama_contact_menu_item($items, $args)
{

    $items .= '
    <div class="text-white d-block d-xl-none mt-3">
        ' . panama_get_language_switcher(true, true) . '
    </div>
    ';

    return $items;
}

add_filter('edit_post_link', 'panama_edit_post_link', 10, 3);
function panama_edit_post_link($link, $post_id, $text = null)
{
    $post = get_post($post_id);
    if (!$post) return;

    $url = get_edit_post_link($post->ID);
    if (!$url) return;

    // if (null === $text)
    $text = '<svg class="icon icon-edit"><use xlink:href="#icon-edit"></use></svg>';

    $link = '<a class="edit-link" href="' . esc_url($url) . '">' . $text . '</a>';
    return $link;
};


add_action('wp_enqueue_scripts', 'panama_load_scripts');
function panama_load_scripts()
{
    wp_enqueue_style('panama_style', get_stylesheet_uri());
    wp_enqueue_script('jquery');
    wp_enqueue_script('panama_js', get_template_directory_uri() . '/js/script.js', array("jquery"));
}

add_filter('document_title_separator', 'panama_document_title_separator');
function panama_document_title_separator($sep)
{
    $sep = '|';
    return $sep;
}

add_filter('the_title', 'panama_title');
function panama_title($title)
{
    if ($title == '') {
        return '...';
    } else {
        return $title;
    }
}

add_filter('the_content_more_link', 'panama_read_more_link');
function panama_read_more_link()
{
    if (!is_admin()) {
        return ' <a href="' . esc_url(get_permalink()) . '" class="more-link">...</a>';
    }
}

add_filter('excerpt_more', 'panama_excerpt_read_more_link');
function panama_excerpt_read_more_link($more)
{
    if (!is_admin()) {
        global $post;
        return ' <a href="' . esc_url(get_permalink($post->ID)) . '" class="more-link">...</a>';
    }
}

add_action('widgets_init', 'panama_widgets_init');
function panama_widgets_init()
{
    register_sidebar(array(
        'name' => esc_html__('Footer', 'phl'),
        'id' => 'widget-footer',
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="d-none" aria-hidden="true">',
        'after_title' => '</div>',
    ));
}

add_action('customize_register', 'panama_customize_register');
function panama_customize_register($wp_customize)
{
    $wp_customize->add_setting('header_image', array('default' => ''));
    $wp_customize->add_setting('tourism_header_image', array('default' => ''));

    $wp_customize->add_section('header', array(
        'title'      => __('Header', 'panama'),
        'priority'   => 30,
    ));

    $wp_customize->add_control(new WP_Customize_Cropped_Image_Control($wp_customize, 'header_image', array(
        'label'      => __('Homepage header', 'panama'),
        'section'    => 'header',
        'settings'   => 'header_image',

        'width' => 1920,
        'height' => 790
    )));

    $wp_customize->add_control(new WP_Customize_Cropped_Image_Control($wp_customize, 'tourism_header_image', array(
        'label'      => __('Tourism page header', 'panama'),
        'section'    => 'header',
        'settings'   => 'tourism_header_image',

        'width' => 1920,
        'height' => 790
    )));

    $wp_customize->add_setting('home_title', array('default' => ''));
    $wp_customize->add_setting('home_image', array('default' => ''));
    $wp_customize->add_setting('values', array('default' => array()));

    $wp_customize->add_section('homepage', array(
        'title'      => __('Homepage', 'panama'),
        'priority'   => 30,
    ));

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'home_title', array(
        'label'      => __('Homepage title', 'panama'),
        'type'       => 'text',
        'section'    => 'homepage',
        'settings'   => 'home_title',
    )));

    $wp_customize->add_control(new WP_Customize_Cropped_Image_Control($wp_customize, 'home_image', array(
        'label'      => __('Home image', 'panama'),
        'section'    => 'homepage',
        'settings'   => 'home_image',

        'width' => 550,
        'height' => 310
    )));


    $wp_customize->add_setting('phone', array('default'   => ''));
    $wp_customize->add_setting('email', array('default'   => ''));
    $wp_customize->add_setting('name', array('default'   => ''));
    $wp_customize->add_setting('street', array('default'   => ''));
    $wp_customize->add_setting('city', array('default'   => ''));
    $wp_customize->add_setting('ico', array('default'   => ''));
    $wp_customize->add_setting('dic', array('default'   => ''));
    $wp_customize->add_setting('phone', array('default'   => ''));
    $wp_customize->add_setting('email', array('default'   => ''));


    $wp_customize->add_section('contact', array(
        'title'      => __('Contact', 'panama'),
        'priority'   => 30,
    ));

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'name', array(
        'label'      => __('Business name', 'panama'),
        'type'       => 'text',
        'section'    => 'contact',
        'settings'   => 'name',
    )));

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'street', array(
        'label'      => __('Street', 'panama'),
        'type'       => 'text',
        'section'    => 'contact',
        'settings'   => 'street',
    )));

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'city', array(
        'label'      => __('Postal code and city', 'panama'),
        'type'       => 'text',
        'section'    => 'contact',
        'settings'   => 'city',
    )));


    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'ico', array(
        'label'      => __('IČO', 'panama'),
        'type'       => 'text',
        'section'    => 'contact',
        'settings'   => 'ico',
    )));

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'dic', array(
        'label'      => __('DIČ', 'panama'),
        'type'       => 'text',
        'section'    => 'contact',
        'settings'   => 'dic',
    )));

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'email', array(
        'label'      => __('Email', 'panama'),
        'type'       => 'email',
        'section'    => 'contact',
        'settings'   => 'email',
    )));

    $wp_customize->add_control(new WP_Customize_Control($wp_customize, 'phone', array(
        'label'      => __('Phone number', 'panama'),
        'type'       => 'text',
        'section'    => 'contact',
        'settings'   => 'phone',
    )));
}

add_filter('kirki/fields', 'panama_kirki_fields');
function panama_kirki_fields($wp_customize)
{
    global $icons;

    $fields[] = array(
        'type'        => 'repeater',
        'label'       => esc_html__('Values', 'panama'),
        'section'     => 'homepage',
        'row_label' => [
            'type'  => 'text',
            'value' => esc_html__('Value', 'panama'),
        ],
        'button_label' => esc_html__('Add new', 'panama'),
        'settings'     => 'values',
        'fields' => [
            'title' => [
                'type'        => 'text',
                'label'       => esc_html__('Title', 'panama'),
                'default'     => '',
            ],
            'text'  => [
                'type'        => 'text',
                'label'       => esc_html__('Text', 'panama'),
                'default'     => '',
            ],
            'icon' =>     [
                'type'        => 'select',
                'label'       => esc_html__('Icon', 'panama'),
                'choices'     => $icons
            ]
        ]
    );

    return $fields;
}

add_action('init', 'panama_register_customize_translations');
function panama_register_customize_translations()
{
    if (!function_exists('pll_register_string')) return;

    $values = get_theme_mod("values");

    if (is_array($values)) {
        foreach ($values as $i => $value) {
            pll_register_string('Homepage value #' . $i . ' - Title', $value["title"], 'Panama');
            pll_register_string('Homepage value #' . $i . ' - Text', $value["text"], 'Panama');
        }
    }
}

/**
 * Outputs localized string if polylang exists or  output's not translated one as a fallback
 *
 * @param $string
 *
 * @return  void
 */
function pl_e($string = '')
{
    if (function_exists('pll_e')) {
        pll_e($string);
    } else {
        echo $string;
    }
}

/**
 * Returns translated string if polylang exists or  output's not translated one as a fallback
 *
 * @param $string
 *
 * @return string
 */
function pl__($string = '')
{
    if (function_exists('pll__')) {
        return pll__($string);
    }

    return $string;
}


/**
 * Taxonomy show_on filter
 * @author Bill Erickson
 * @param  object $cmb CMB2 object
 * @return bool        True/false whether to show the metabox
 */
function be_taxonomy_show_on_filter($cmb)
{
    $tax_terms_to_show_on = $cmb->prop('show_on_terms', array());
    if (empty($tax_terms_to_show_on) || !$cmb->object_id()) {
        return false;
    }

    $post_id = $cmb->object_id();
    $post = get_post($post_id);

    foreach ((array) $tax_terms_to_show_on as $taxonomy => $slugs) {
        if (!is_array($slugs)) {
            $slugs = array($slugs);
        }

        $terms = $post
            ? get_the_terms($post, $taxonomy)
            : wp_get_object_terms($post_id, $taxonomy);

        if (!empty($terms)) {
            foreach ($terms as $term) {
                if (in_array($term->slug, $slugs, true)) {
                    //wp_die('<xmp>: ' . print_r('show it', true) . '</xmp>');
                    // Ok, show this metabox
                    return true;
                }
            }
        }
    }

    return false;
}

// REMOVE COMMENTS
add_action('admin_menu', 'remove_comments_menu');
function remove_comments_menu()
{
    remove_menu_page('edit-comments.php');
}

// Removes from post and pages
add_action('init', 'remove_comments_support', 100);
function remove_comments_support()
{
    remove_post_type_support('post', 'comments');
    remove_post_type_support('page', 'comments');
}

// Removes from admin bar
add_action('wp_before_admin_bar_render', 'remove_toolbar_comments_menu');
function remove_toolbar_comments_menu()
{
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('comments');
}

// REMOVE POSTS 1/3
add_action('admin_menu', 'remove_posts_menu');
function remove_posts_menu()
{
    remove_menu_page('edit.php');
}

// REMOVE POSTS 2/3
add_action('wp_before_admin_bar_render', 'remove_toolbar_posts_menus');
function remove_toolbar_posts_menus()
{
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('new-post');
}

// REMOVE POSTS 3/3
add_action('wp_dashboard_setup', 'remove_dashboard_widgets');
function remove_dashboard_widgets()
{
    remove_action('welcome_panel', 'wp_welcome_panel');

    global $wp_meta_boxes;
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['network_dashboard_right_now']);
}

// REMOVE CONTACT FORM ADMIN PAGE
add_action('admin_menu', 'remove_contactform7_admin');
function remove_contactform7_admin()
{
    remove_menu_page('admin.php?page=wpcf7');
}

add_action('init', 'remove_editor');
function remove_editor()
{
    if (isset($_GET['post'])) {
        $id = $_GET['post'];
        $template = get_post_meta($id, '_wp_page_template', true);
        switch ($template) {
            case 'templates/tpl-cars.php':
                remove_post_type_support('page', 'editor');
            default:
                break;
        }
    }
}

function get_meta_wisywig($meta_key, $post_id = 0)
{
    global $wp_embed;

    $post_id = $post_id ? $post_id : get_the_id();

    $content = get_post_meta($post_id, $meta_key, 1);
    $content = $wp_embed->autoembed($content);
    $content = $wp_embed->run_shortcode($content);
    $content = wpautop($content);
    $content = do_shortcode($content);

    return $content;
}

function the_meta_wisywig($meta_key, $post_id = 0)
{
    echo get_meta_wisywig($meta_key, $post_id);
}

add_filter('cmb2_meta_box_url', 'update_cmb2_meta_box_url');
function update_cmb2_meta_box_url($url)
{
    return get_template_directory_uri() . "/cmb2";
}

function get_meta_text($meta_key, $post_id = 0)
{
    return get_post_meta(get_the_ID(), $meta_key, 1);
}

function the_meta_text($meta_key, $post_id = 0)
{
    echo get_meta_text($meta_key, $post_id);
}


function the_meta_slider($file_list_meta_key, $width, $height, $img_size = 'medium-cropped')
{

    // Get the list of files
    $files = get_post_meta(get_the_ID(), $file_list_meta_key, 1);

    echo '<div class="image-slider" style="height: ' . $height . '; width: ' . $width . ';">';
    // Loop through them and output an image
    $first = true;

    foreach ((array) $files as $attachment_id => $attachment_url) {
        echo "<div style=\"background-image: url('";
        echo wp_get_attachment_image_url($attachment_id, $img_size);
        echo "');\"" . (($first) ? 'class="active"' : "") . "></div>";

        $first = false;
    }

    echo '</div>';
}

function get_image_by_id($attachment_id, $img_size, $class = "")
{
    $src = wp_get_attachment_image_url($attachment_id, $img_size);
    $alt = wp_get_attachment_caption($attachment_id);

    return "<img src='$src' alt='$alt' class='$class'>";
}

function the_image_by_id($attachment_id, $img_size, $class = "")
{
    echo get_image_by_id($attachment_id, $img_size, $class);
}

function the_nth_meta_img($file_list_meta_key, $index, $img_size = 'medium-cropped', $class = "")
{
    $files = get_post_meta(get_the_ID(), $file_list_meta_key, 1);

    if (sizeof((array) $files) < ($index + 1)) return 0;

    $attachment_id = array_keys($files)[$index];

    the_image_by_id($attachment_id, $img_size, $class);
}

function get_subtitle()
{
    return get_post_meta(get_the_ID(), "subtitle", 1);
}

function the_subtitle()
{
    echo get_post_meta(get_the_ID(), "subtitle", 1);
}

function format_top_title(string $title)
{
    $words = explode(" ", $title);

    if (sizeof($words) == 2) {
        return "{$words[0]} <span class='font-weight-normal'>{$words[1]}</span>";
    } elseif (sizeof($words) == 3) {
        return "{$words[0]} {$words[1]} <span class='font-weight-normal'>{$words[2]}</span>";
    }

    return $title;
}

function panama_get_language_switcher(bool $hide_current = false, bool $full_name = false): string
{
    $language_switcher = "";
    if (function_exists("pll_the_languages")) {
        $translations = pll_the_languages(array('raw' => 1, 'hide_if_empty' => 0, 'hide_current' => $hide_current));

        if ($full_name)
            $lang_links = array_map(function ($lang) {
                return "<a href='${lang['url']}' class='text-light text-uppercase mx-2'>${lang['name']}</a>";
            }, $translations);
        else
            $lang_links = array_map(function ($lang) {
                return "<a href='${lang['url']}' class='text-light text-uppercase mx-2'>${lang['slug']}</a>";
            }, $translations);

        $language_switcher = join("/", $lang_links);
    }

    return $language_switcher;
}

function panama_language_switcher(bool $hide_current = false, bool $full_name = false): void
{
    echo panama_get_language_switcher($hide_current, $full_name);
}


function is_project()
{
    global $wp_query;
    return array_key_exists('project', $wp_query->query_vars);
}

function the_gallery($file_list_meta_key = "panama_gallery_files")
{
    $files = get_post_meta(get_the_ID(), $file_list_meta_key, 1);

    if (sizeof($files) > 0) {
        echo '<figure class="wp-block-gallery columns-5 is-cropped">';
        echo '<ul class="blocks-gallery-grid">';

        foreach ((array) $files as $attachment_id => $attachment_url) {
            echo '<li class="blocks-gallery-item">';
            echo '<figure>';
            echo '<a class="image-hover" data-gallery-id="' . $file_list_meta_key . '" href="' . wp_get_attachment_image_url($attachment_id, 'large') . '">';
            echo wp_get_attachment_image($attachment_id, 'full', false);
            echo '</a>';
            echo '</figure>';
            echo '</li>';
        }

        echo '</ul>';
        echo '</figure>';
    }
}
