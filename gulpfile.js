/******************************************************
 * PATTERN LAB NODE
 * EDITION-NODE-GULP
 * The gulp wrapper around patternlab-node core, providing tasks to interact with the core library.
 ******************************************************/
const gulp = require('gulp');
const argv = require('minimist')(process.argv.slice(2));
const path = require('path');

const gulpBabel = require('gulp-babel');
const gulpSass = require('gulp-sass');
const gulpSassGlob = require('gulp-sass-glob');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const rename = require("gulp-rename");
const gap = require('gulp-append-prepend');
const open = require('open');
const zip = require('gulp-zip');

const compose = require('docker-compose');
const proxy = require('hot-reloading-proxy/server');

const merge = require('merge-stream');
const chalk = require('chalk');
const run = require('gulp-run');

require('dotenv').config();

const package = require('./package.json')
const themeDomain = package.name

/**
 * Normalize all paths to be plain, paths with no leading './',
 * relative to the process root, and with backslashes converted to
 * forward slashes. Should work regardless of how the path was
 * written. Accepts any number of parameters, and passes them along to
 * path.resolve().
 *
 * This is intended to avoid all known limitations of gulp.watch().
 *
 * @param {...string} pathFragment - A directory, filename, or glob.
*/
function normalizePath() {
    return path
        .relative(
            process.cwd(),
            path.resolve.apply(this, arguments)
        )
        .replace(/\\/g, "/");
}


/******************************************************
 * PATTERN LAB
 ******************************************************/
const config = require('./patternlab-config.json');
const patternlab = require('@pattern-lab/core')(config);

gulp.task('patternlab:watch', function (done) {
    patternlab
        .build({
            watch: true,
            cleanPublic: config.cleanPublic,
        })
        .then(() => done())
        .catch((e) => done(e));
})

gulp.task('patternlab:build', function (done) {
    patternlab
        .build({
            watch: argv.watch,
            cleanPublic: config.cleanPublic,
        })
        .then(() => done())
        .catch((e) => done(e));
})

gulp.task('patternlab:serve', function (done) {
    patternlab.server
        .serve({
            cleanPublic: config.cleanPublic,
            watch: true,
        })
        .then(() => done())
        .catch((e) => done(e));
});

gulp.task('patternlab:version', function (done) {
    console.log(patternlab.version())
    done()
});

gulp.task('patternlab:patternsonly', function (done) {
    patternlab
        .patternsonly(config.cleanPublic)
        .then(() => done())
        .catch((e) => done(e));
});

gulp.task('patternlab:liststarterkits', function (done) {
    patternlab
        .liststarterkits()
        .then(() => done())
        .catch((e) => done(e));
});

gulp.task('patternlab:loadstarterkit', function (done) {
    patternlab.
        loadstarterkit(argv.kit, argv.clean)
        .then(() => done())
        .catch((e) => done(e));
});

/******************************************************
 * SASS
 ******************************************************/

function sass() {
    return gulp.src('*.scss', { cwd: normalizePath(config.paths.source.css) })
        .pipe(gulpSassGlob())
        .pipe(gulpSass().on('error', gulpSass.logError))
        .pipe(autoprefixer())
        .pipe(cleanCSS())
        // .pipe(rename(config.compiledStyleName))
        .pipe(gulp.dest(normalizePath(config.paths.source.css)));
}

gulp.task('sass', sass);

gulp.task('sass:watch', function () {
    gulp.watch([
        normalizePath(config.paths.source.css, '**', '*.scss'),
        normalizePath(config.paths.source.patterns, '**', '*.scss')
    ], sass);
});

/******************************************************
 * BABEL
 ******************************************************/

function babel() {
    return gulp.src('**/*.js', { cwd: normalizePath(config.paths.compile.js) })
        .pipe(gulpBabel({ presets: ['@babel/env'] }))
        .pipe(gulp.dest(normalizePath(config.paths.source.js)));
}

gulp.task('babel', babel);

gulp.task('babel:watch', function () {
    gulp.watch([normalizePath(config.paths.compile.js, '**', '*.js')], babel);
});

/******************************************************
 * DOCKER
 ******************************************************/

gulp.task('docker:start', function (done) {
    compose.upAll({ cwd: path.join(__dirname), log: true })
        .then(() => done())
        .catch((e) => done(e));
})

gulp.task('docker:stop', function (done) {
    compose.down({ cwd: path.join(__dirname), log: true })
        .then(() => done())
        .catch((e) => done(e));
})

/******************************************************
 * WORDPRESS
 ******************************************************/

function createThemeHeader() {
    const fields = [
        ["Theme Name", package.displayName || package.name],
        ["Description", package.description || ""],
        ["Version", package.version || "1.0.0"],
        ["Text Domain", package.name]
    ]

    const lines = fields.map(function (field) {
        return field.join(": ")
    })

    const header = lines.join("\n")

    return "/*\n" + header + "\n*/"
}

gulp.task('wp:css', function () {
    return gulp.src("*.css", { cwd: normalizePath(config.paths.source.css) })
        .pipe(gap.prependText(getThemeHeader()))
        .pipe(gulp.dest(normalizePath(config.paths.theme.css)));
});

gulp.task('wp:watch-css', function () {
    gulp.watch([normalizePath(config.paths.source.css, "*.css")], gulp.task('wp:css'));
});

gulp.task('wp:js', function () {
    return gulp.src('**/*.js', { cwd: normalizePath(config.paths.source.js) })
        .pipe(gulp.dest(normalizePath(config.paths.theme.js)));
});

gulp.task('wp:watch-js', function () {
    gulp.watch([normalizePath(config.paths.source.js, '**', '*.js')], gulp.task('wp:js'));
});

// Images copy
gulp.task('wp:img', function () {
    return gulp.src('**/*.*', { cwd: normalizePath(config.paths.source.images) })
        .pipe(gulp.dest(normalizePath(config.paths.theme.images)));
});

gulp.task('wp:watch-img', function () {
    gulp.watch([normalizePath(config.paths.source.images, '**', '*.*')], gulp.task('wp:img'));
});

// Favicon copy
gulp.task('wp:favicon', function () {
    return gulp.src('favicon.ico', { cwd: normalizePath(config.paths.source.root) })
        .pipe(gulp.dest(normalizePath(config.paths.theme.root)));
});

gulp.task('wp:assets', gulp.parallel('wp:css', 'wp:js', 'wp:img', 'wp:favicon'));
gulp.task('wp:watch', gulp.parallel('wp:watch-css', 'wp:watch-js', 'wp:watch-img'))

gulp.task('wp:proxy', function (done) {
    const url = "http://localhost:" + process.env.PORT
    const port = process.env.PROXY_PORT
    proxy
        .start({
            remote: url,
            port: port,
            watch: normalizePath(config.paths.theme.root)
        })
        .then(() => {
            open("http://localhost:" + port, { wait: false });
            done();
        })
        .catch((e) => done(e));
})

/******************************************************
 * WP Plugins
******************************************************/

function getPlugins() {
    return Object.keys(config.wpPlugins);
}

function pluginDir(key) {
    return config.wpPlugins[key]
}

function pluginPaths(plugin) {
    const dir = pluginDir(plugin)
    return [
        normalizePath(dir, plugin + '.php'),
        // normalizePath(dir, 'block.json'),
        normalizePath(dir, 'build', '**', '*'),
        normalizePath(dir, 'languages', '**', '*'),
        normalizePath(dir, 'assets', '**', '*')
    ]
}

function buildPlugin(plugin, mode = "development") {
    const dir = pluginDir(plugin);
    console.log(chalk.blue("Starting build of plugin " + plugin))

    return run('NODE_ENV=' + mode + ' npm run wp-scripts build ' + normalizePath(dir, 'src', 'index.js') + ' -- --output-path=' + normalizePath(dir, 'build')).exec()
}

// function watchPlugin(plugin) {
//     const dir = pluginDir(plugin);
//     console.log(chalk.yellow("Starting plugin " + plugin))

//     return run('npm run wp-scripts start ' + normalizePath(dir, 'src', 'index.js') + ' -- --webpack--devtool=source-map -- --output-path=' + normalizePath(dir, 'build')).exec()
// }

function copyPlugin(plugin) {
    const dir = pluginDir(plugin);

    console.log(chalk.cyan("Copy plugin " + plugin))

    return merge(
        gulp.src(normalizePath(dir, plugin + '.php'))
            .pipe(gulp.dest(normalizePath(config.paths.theme.plugins, plugin))),
        gulp.src(normalizePath(dir, 'src', '**', '*.php'))
            .pipe(gulp.dest(normalizePath(config.paths.theme.plugins, plugin, 'build'))),
        gulp.src(normalizePath(dir, 'build', '**', '*'))
            .pipe(gulp.dest(normalizePath(config.paths.theme.plugins, plugin, 'build'))),
        gulp.src(normalizePath(dir, 'languages', '**', '*'))
            .pipe(gulp.dest(normalizePath(config.paths.theme.plugins, plugin, 'languages'))),
        gulp.src(normalizePath(dir, 'assets', '**', '*'))
            .pipe(gulp.dest(normalizePath(config.paths.theme.plugins, plugin, 'assets')))
    )
}

gulp.task('wp:plugins:copy', function (done) {
    const plugins = getPlugins()
    if (plugins.length == 0) return done()

    return merge(plugins.map(function (plugin) {
        return copyPlugin(plugin)
    }))
})

gulp.task('wp:plugins:build', function (done) {
    const plugins = getPlugins()
    if (plugins.length == 0) return done()

    return merge(plugins.map(function (plugin) {
        return buildPlugin(plugin, "development")
    }))
})

gulp.task('wp:plugins:watch', function (done) {
    const plugins = getPlugins()
    if (plugins.length == 0) return done()

    const copyWatchers = plugins.map(function (plugin) {
        return {
            name: 'copy plugin ' + plugin,
            paths: pluginPaths(plugin),
            config: { awaitWriteFinish: true },
            tasks: gulp.series(
                function copy() { return copyPlugin(plugin) }
            )
        }
    })

    const buildWatchers = plugins.map(function (plugin) {
        const dir = pluginDir(plugin);
        return {
            name: 'build plugin ' + plugin,
            paths: [normalizePath(dir, 'src', '**', '*'),],
            config: { awaitWriteFinish: true },
            tasks: gulp.series(
                function build() { return buildPlugin(plugin) }
            )
        }
    })

    const watchers = [...copyWatchers, ...buildWatchers]

    watchers.forEach(watcher => {
        console.log('\n' + chalk.yellow('Watching ' + watcher.name + ':'));
        watcher.paths.forEach(p => console.log('  ' + p));
        gulp.watch(watcher.paths, watcher.config, watcher.tasks);
    });

    console.log();

    // return merge(plugins.map(function (plugin) {
    //     return watchPlugin(plugin)
    // }))
})

gulp.task('wp:plugins', gulp.series('wp:plugins:build', 'wp:plugins:copy'))

/******************************************************
 * ZIP
 ******************************************************/

gulp.task('zip', function () {
    return gulp.src(normalizePath(config.paths.theme.root, '**', '*'))
        .pipe(rename(function (path) {
            path.dirname = themeDomain + "/" + path.dirname
        }))
        .pipe(zip(themeDomain + '.zip'))
        .pipe(gulp.dest(normalizePath(config.paths.dist)));
})

/******************************************************
 * DEFAULT TASKS
 ******************************************************/

gulp.task('default', gulp.task('build'));

gulp.task('wp:serve', gulp.series(gulp.parallel('sass', 'babel'), 'patternlab:build', 'wp:assets', 'docker:start', 'wp:plugins:copy', gulp.parallel('sass:watch', 'babel:watch', 'wp:watch', 'wp:proxy', 'wp:plugins:watch')));
gulp.task('patternlab:serve', gulp.series(gulp.parallel('sass', 'babel'), 'patternlab:serve', gulp.parallel('babel:watch', 'sass:watch')));


gulp.task('build', gulp.series(gulp.parallel('sass', 'babel'), 'patternlab:build', 'wp:assets', 'wp:plugins'))

gulp.task('wp:bundle', gulp.series('build', 'zip'))
