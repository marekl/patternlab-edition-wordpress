### STAGE 1: Build theme ###
FROM node:lts-alpine as builder
RUN apk --no-cache add curl python3 py3-pip make g++

WORKDIR /app
COPY package*.json ./

RUN echo "@marekl:registry=https://npm.marekl.cz/" > .npmrc

# Install node packages, copy our app and build it
RUN npm ci
COPY . .

RUN node -p "'THEME_DOMAIN='+(/^(?:@.*?\/)?(.*)$/).exec(require('./package.json').name)[1]" > domain

RUN npm run build
RUN curl https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar -o /usr/local/bin/wp && chmod +x /usr/local/bin/wp

### STAGE 2: Wordpress ###
FROM wordpress:latest

RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive \
    apt-get -y --quiet --no-install-recommends install \
    less \
    && rm -rf /var/lib/apt/lists/*

RUN sed -i "/WP_DEBUG/a if ( (!empty( \$_SERVER['HTTP_X_FORWARDED_HOST'])) || (!empty( \$_SERVER['HTTP_X_FORWARDED_FOR'])) ) { \$_SERVER['HTTP_HOST'] = \$_SERVER['HTTP_X_FORWARDED_HOST']; }" /usr/src/wordpress/wp-config-sample.php

RUN echo "file_uploads = On\nmemory_limit = 64M\nupload_max_filesize = 64M\npost_max_size = 64M\nmax_execution_time = 600" > /usr/local/etc/php/conf.d/uploads.ini

COPY --from=builder /usr/local/bin/wp /usr/local/bin/wp
COPY --from=builder /app/theme /usr/src/wp-theme
COPY --from=builder /app/domain /tmp/domain

RUN . /tmp/domain && chown -R www-data:www-data /usr/src/wp-theme && \
    ln -sf /usr/src/wp-theme /usr/src/wordpress/wp-content/themes/$THEME_DOMAIN && \
    chown -R www-data:www-data /usr/src/wordpress
