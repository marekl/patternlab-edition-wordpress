# Patternlab Wordpress edition

Sample ENV config:

```env
MYSQL_ROOT_PASSWORD=root

MYSQL_DATABASE=wp
MYSQL_USER=wp
MYSQL_PASSWORD=wp

PORT=8080
PROXY_PORT=8081
```
